use movie_db;
CREATE TABLE publications (name VARCHAR(250) NOT NULL, avatar VARCHAR(250), PRIMARY KEY (name));
CREATE TABLE reviewers (name VARCHAR(255) NOT NULL, avatar VARCHAR(250), publication VARCHAR(250), PRIMARY KEY (name), FOREIGN KEY (publication) REFERENCES publications(name));
CREATE TABLE movies (title VARCHAR(250) NOT NULL, release_year VARCHAR(250), score INT(11), reviewer VARCHAR(250), publication VARCHAR(250), PRIMARY KEY (title), FOREIGN KEY (reviewer) REFERENCES reviewers(name));

INSERT INTO publications (name, avatar) VALUES ('The Daily Reviewer', 'glyphicon-eye-open');
INSERT INTO publications (name, avatar) VALUES ('International Movie Critic', 'glyphicon-fire');
INSERT INTO publications (name, avatar) VALUES ('MoviesNow', 'glyphicon-time');
INSERT INTO publications (name, avatar) VALUES ('MyNextReview', 'glyphicon-record');
INSERT INTO publications (name, avatar) VALUES ('Movies n\' Games', 'glyphicon-heart-empty');
INSERT INTO publications (name, avatar) VALUES ('TheOne', 'glyphicon-globe');
INSERT INTO publications (name, avatar) VALUES ('ComicBookHero.com', 'glyphicon-flash');

INSERT INTO reviewers (name, publication, avatar) VALUES ('Robert Smith', 'The Daily Reviewer', 'https://s3.amazonaws.com/uifaces/faces/twitter/angelcolberg/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Chris Harris', 'International Movie Critic', 'https://s3.amazonaws.com/uifaces/faces/twitter/bungiwan/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Janet Garcia', 'MoviesNow', 'https://s3.amazonaws.com/uifaces/faces/twitter/grrr_nl/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Andrew West', 'MyNextReview', 'https://s3.amazonaws.com/uifaces/faces/twitter/d00maz/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Mindy Lee', 'Movies n\' Games', 'https://s3.amazonaws.com/uifaces/faces/twitter/laurengray/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Martin Thomas', 'TheOne', 'https://s3.amazonaws.com/uifaces/faces/twitter/karsh/128.jpg');
INSERT INTO reviewers (name, publication, avatar) VALUES ('Anthony Miller', 'ComicBookHero.com', 'https://s3.amazonaws.com/uifaces/faces/twitter/9lessons/128.jpg');

INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Suicide Squad', '2016', 8, 'Robert Smith', 'The Daily Reviewer');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Batman vs. Superman', '2016', 6, 'Chris Harris', 'International Movie Critic');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Captain America: Civil War', '2016', 9, 'Janet Garcia', 'MoviesNow');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Deadpool', '2016', 9, 'Andrew West', 'MyNextReview');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Avengers: Age of Ultron', '2015', 7, 'Mindy Lee', 'Movies n\' Games');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Ant-Man', '2015', 8, 'Martin Thomas', 'TheOne');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Guardians of the Galaxy', '2014', 10, 'Anthony Miller', 'ComicBookHero.com');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Doctor Strange', '2016', 7, 'Anthony Miller', 'ComicBookHero.com');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Superman: Homecoming', '2017', 10, 'Chris Harris', 'International Movie Critic');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('Wonder Woman', '2017', 8, 'Martin Thomas', 'TheOne');
INSERT INTO movies (title, release_year, score, reviewer, publication) VALUES ('The Godfather', '1972', 10, 'Martin Thomas', 'TheOne');
