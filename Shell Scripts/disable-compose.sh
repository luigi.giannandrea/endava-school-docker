#!/bin/bash
echo ---------------------------------------------------------
echo Endava - School of Devops - Virtualization and Containers
echo Luigi Giannandrea
echo ---------------------------------------------------------
echo Docker Compose - Removing Services
echo ---------------------------------------------------------
sudo docker compose -f /vagrant/docker-compose.yml down
sudo docker volume rm vagrant_db_data
echo Done!
