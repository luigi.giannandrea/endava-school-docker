#!/bin/bash
echo -------------------------------------------------------------
echo Endava - School of Devops - Virtualization and Containers
echo Luigi Giannandrea
echo -------------------------------------------------------------
echo Docker Compose - Creating Services Mysql - Backend - Frontend
echo -------------------------------------------------------------
sudo docker compose -f /vagrant/docker-compose.yml up -d
echo Done
