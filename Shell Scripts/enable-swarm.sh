#!/bin/bash
echo ---------------------------------------------------------
echo Endava - School of Devops - Virtualization and Containers
echo Luigi Giannandrea
echo ---------------------------------------------------------
echo Docker Swarm - Creating Services
echo   Mysql    1 replica
echo   Backend  2 replicas - 1 per node
echo   Frontend 2 replicas - 1 per node
echo ---------------------------------------------------------
sudo docker service create --publish published=3306,target=3306 -e MYSQL_ROOT_PASSWORD=school -e MYSQL_DATABASE=movie_db -e MYSQL_USER=school -e MYSQL_PASSWORD=school --name mysqlserver mysql:school
sudo docker service create --replicas 2 --replicas-max-per-node 1 --publish published=8080,target=3333 -e DB_HOST=192.168.56.2 -e DB_USER=school -e DB_PASS=school -e DB_NAME=movie_db --name backend api:school
sudo docker service create --replicas 2 --replicas-max-per-node 1 --publish published=8082,target=8080 -e PORT=8080 -e BACKEND_URL=192.168.56.2:8080 --name frontend frontend:school
sudo docker service ls
echo Done!
