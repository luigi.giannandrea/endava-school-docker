#!/bin/bash
echo ---------------------------------------------------------
echo Endava - School of Devops - Virtualization and Containers
echo Luigi Giannandrea
echo ---------------------------------------------------------
echo Docker Swarm - Removing Services
echo ---------------------------------------------------------
for i in `sudo docker service ls | awk '{ print $1}' | grep -vi ID`; do sudo docker service rm $i; done
echo Done!
